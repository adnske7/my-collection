﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Chaos
{
    public class NullPointException : Exception
    {
        public NullPointException()
        {
            Console.WriteLine("The slot that was checked was empty");
        }

       
    }
}
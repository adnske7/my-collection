﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Chaos
{
    class ChaosArray<T>
    {
        public T[] chaos;
        Random rndm;

        public ChaosArray()
        {
            chaos = new T[6];
            rndm = new Random();

        }
        public void Insert(T element)
        {
            int random = rndm.Next(chaos.Length);
            if (chaos[random] != null)
            {
                throw new OccupiedElementException();
            }
            else
            {
                chaos[random] = element;
                Console.WriteLine("Placed " + element + " in poition " + random);
            }
        }
        public T Retrive()
        {
            int random = rndm.Next(chaos.Length);
            if (chaos[random] != null)
            {
                return (chaos[random]);
            }
            else
            {
                throw new NullPointException();
            }
        }
    }
}

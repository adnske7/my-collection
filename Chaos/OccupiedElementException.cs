﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Chaos
{
   public class OccupiedElementException : Exception
    {
        public OccupiedElementException()
        {
            Console.WriteLine("Insertion was not possible, slot taken");
        }

    }
}
